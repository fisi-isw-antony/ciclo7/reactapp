import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';

import './index.css';
import * as serviceWorker from './serviceWorker';

import App from './components/App';

// ReactDOM.render(
//     <Badge
//         firstName="Antony"
//         lastName="Montalvo"
//         avatarUrl="https://s.gravatar.com/avatar/fd4855fac43520e947ff2316c5bc47a2?s=80"
//         jobTitle="Software Developer"
//         twitter="antony162010" />, document.getElementById('root'));

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
