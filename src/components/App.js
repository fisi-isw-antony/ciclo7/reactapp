import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Layout from "./Layout";

import BadgePage from "../pages/BadgePage";
import BadgesPage from "../pages/BadgesPage";
import NotFound from "../pages/NotFound";
import HomePage from "../pages/HomePage";

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/badges" component={BadgesPage} />
          <Route exact path="/badges/new" component={BadgePage} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
