import React, { Component } from "react";

import BadgeListElement from "../components/BadgeListElement";

class BadgeList extends Component {
  render() {
    let data = this.props.data;

    return (
      <ul className="list-unstyled">
        {data.map(b => {
          return <BadgeListElement key={b.id} badge={b} />;
        })}
      </ul>
    );
  }
}

export default BadgeList;
