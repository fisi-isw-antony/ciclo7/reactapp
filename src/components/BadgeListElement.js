import React, { Component } from "react";
import { TwitterShareButton, TwitterIcon } from "react-share";

import "./styles/BadgesListElement.css";

class BadgeListElement extends Component {
  render() {
    let badge = this.props.badge;

    return (
      <li className="BadgesListItem bg-light">
        <div className="BadgesList__container">
          <img
            className="BadgesListItem__avatar"
            src={badge.avatarUrl}
            alt="Avatar"
          />
        </div>

        <ul className="list-unstyled size-ul">
          <li>
            {badge.firstName} {badge.lastName}
          </li>
          <li>
            <TwitterShareButton
              className="share-btn"
              title="Lo aprendi en la clase: "
              url='https://platzi.com/clases/1548-react/18703-listas-de-componentes1177/'
              children={
                <TwitterIcon
                  size={32}
                  round={true}
                  logoFillColor="rgb(0, 172, 237)"
                  iconBgStyle={{ fill: "white" }}
                />
              }
            />
            @{badge.twitter}
          </li>
          <li>{badge.jobTitle}</li>
        </ul>
      </li>
    );
  }
}

export default BadgeListElement;
