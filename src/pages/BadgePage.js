import React, { Component } from "react";

import "./styles/BadgePage.css";
import logo from "../images/badge-header.svg";
import Badge from "../components/Badge";
import BadgeForm from "../components/BadgeForm";

class BadgePage extends Component {
  state = {
    form: {
      firstName: "",
      lastName: "",
      email: "",
      jobTitle: "",
      twitter: ""
    }
  };

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  render() {
    let form = this.state.form;

    return (
      <React.Fragment>
        <div className="BadgePage__hero">
          <img className="img-fluid" src={logo} alt="Logo" />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <Badge
                firstName={form.firstName}
                lastName={form.lastName}
                email={form.email}
                avatarUrl="https://s.gravatar.com/avatar/fd4855fac43520e947ff2316c5bc47a2?s=80"
                jobTitle={form.jobTitle}
                twitter={form.twitter}
              />
            </div>

            <div className="col-md-6">
              <BadgeForm
                propChange={this.handleChange}
                formValues={this.state.form}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BadgePage;
